﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public void Awake()
    {
        var centralObject = GameObject.FindGameObjectWithTag("CentralObject");
        camera.transform.LookAt(centralObject.transform.position);
    }

    public void FixedUpdate()
    {
        var centralObject = GameObject.FindGameObjectWithTag("CentralObject");
        var mainObject = GameObject.FindGameObjectWithTag("MainObject");
        if (Input.GetKey("up"))
        {
            mainObject.transform.RotateAround(centralObject.transform.position, Vector3.left, RotateSpeed * Time.deltaTime);
            //transform.RotateAround(centralObject.transform.position, Vector3.left, RotateSpeed * Time.deltaTime);
        }

        if (Input.GetKey("down"))
        {
            mainObject.transform.RotateAround(centralObject.transform.position, Vector3.right, RotateSpeed * Time.deltaTime);
            //transform.RotateAround(centralObject.transform.position, Vector3.right, RotateSpeed * Time.deltaTime);
        }

        if (Input.GetKey("left"))
        {
            mainObject.transform.RotateAround(centralObject.transform.position, Vector3.up, RotateSpeed * Time.deltaTime);
            //transform.RotateAround(centralObject.transform.position, Vector3.up, RotateSpeed * Time.deltaTime);
        }

        if (Input.GetKey("q"))
        {
            mainObject.transform.RotateAround(centralObject.transform.position, Vector3.forward, RotateSpeed * Time.deltaTime);
            //transform.RotateAround(centralObject.transform.position, Vector3.down, RotateSpeed * Time.deltaTime);
        }

        if (Input.GetKey("w"))
        {
            mainObject.transform.RotateAround(centralObject.transform.position, Vector3.back, RotateSpeed * Time.deltaTime);
            //transform.RotateAround(centralObject.transform.position, Vector3.down, RotateSpeed * Time.deltaTime);
        }
        
        if (Input.GetKey("a") && camera.fieldOfView > MinPointOfView)
        {
            camera.fieldOfView -= PointOfViewStep;
        }

        if (Input.GetKey("s") && camera.fieldOfView < MaxPointOfView)
        {
            camera.fieldOfView += PointOfViewStep;
        }

        camera.transform.LookAt(centralObject.transform.position);
    }

    public float RotateSpeed;

    public float MaxPointOfView = 100f;

    public float MinPointOfView = 30f;

    public float PointOfViewStep = 0.025f;
}