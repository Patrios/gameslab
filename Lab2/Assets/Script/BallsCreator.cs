﻿using System.Collections.Generic;
using UnityEngine;

public class BallsCreator : MonoBehaviour
{

    void FixedUpdate()
    {
        if (IsLeftClick() && _selectedBalls.Count == 0)
        {
            Debug.Log("Pressed left click.");

            var position = GetWorldMousePosition();

            if (position.x > ButtomLeftPoint.x &&
                position.x < UpperRightPoint.x &&
                position.y > ButtomLeftPoint.y &&
                position.y < UpperRightPoint.y)
            {
                Debug.Log("Positions are good!");
                Instantiate(BallPFB, new Vector3(position.x, position.y, -0.25f), Quaternion.Euler(90.0f, 0, 0));
            }
        }

        if (IsLeftClick() && _selectedBalls.Count > 0)
        {
            var position = GetWorldMousePosition();

            _selectedBalls.ForEach(ball =>
            {
                ball.renderer.material.color = Color.white;

                var powerVector = new Vector2(
                    position.x - ball.transform.position.x,
                    position.y - ball.transform.position.y)*Power;

                Debug.Log(powerVector.ToString());

                ball.rigidbody2D.AddForce(powerVector);
            });

            _selectedBalls.Clear();
        }

        if (IsRightClick())
        {
            var position = GetWorldMousePosition();
            var hitted = Physics2D.Raycast(new Vector2(position.x, position.y), Vector2.zero, 0);

            if (hitted.collider != null)
            {
                if (hitted.collider.transform.gameObject.tag == "Ball")
                {
                    _selectedBalls.Add(hitted.collider.transform.gameObject);

                    hitted.collider.renderer.material.color = Color.cyan;
                }
            }
        }
    }

    private Vector3 GetWorldMousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private bool IsLeftClick()
    {
        return Input.GetMouseButtonUp(0);
    }

    private bool IsRightClick()
    {
        return Input.GetMouseButtonUp(1);
    }

    public Vector2 ButtomLeftPoint;
    public Vector2 UpperRightPoint;
    public GameObject BallPFB;
    public float Power;

    private List<GameObject> _selectedBalls = new List<GameObject>();
}
