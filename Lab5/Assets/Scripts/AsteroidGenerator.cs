﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidGenerator : MonoBehaviour
{
    private void Awake()
    {
        _privGen = DateTime.Now;
    }
    // Update is called once per frame
    private void FixedUpdate()
    {
        CollectAsteroids();
        GenerateAsteroids();
    }

    private void GenerateAsteroids()
    {
        if (DateTime.Now - _privGen > GenerationDelta &&
            Random.Range(0, 100) < GenerationChance &&
            _currentAsteroidsCount < MaxCount)
        {
            _currentAsteroidsCount++;
            var zPosition = Random.Range(
                Camera.main.transform.position.z + GenerationDistance,
                Camera.main.transform.position.z + 2 * GenerationDistance);

            var yPosition = Random.Range(BottomLeftGenerationPoint.y, UpperRightGenerationPoint.y);
            var xPosition = Random.Range(BottomLeftGenerationPoint.x, UpperRightGenerationPoint.x);

            var asteroid = Instantiate(Asteroid, new Vector3(xPosition, yPosition, zPosition), Random.rotation);
            var asterObj = asteroid as GameObject;

            asterObj.transform.localScale = new Vector3(
                Random.Range(0.025f, 0.04f),
                Random.Range(0.025f, 0.04f),
                Random.Range(0.025f, 0.04f));
        }
    }

    private void CollectAsteroids()
    {
        var asteroids = GameObject
            .FindGameObjectsWithTag("Asteroid")
            .Where(ball => ball.transform.position.z + TrashDelta < Camera.main.transform.position.z);

        foreach (var asteroid in asteroids)
        {
            Destroy(asteroid);
            _currentAsteroidsCount--;
        }
    }

    public float TrashDelta = 1f;

    public GameObject Asteroid;
    public int GenerationDistance = 50;
    public int MaxCount = 10;

    public Vector2 BottomLeftGenerationPoint = new Vector2(-5f, -3f);
    public Vector2 UpperRightGenerationPoint = new Vector2(5f, 3f);

    public int GenerationChance = 5;

    public TimeSpan GenerationDelta = TimeSpan.FromSeconds(2);

    private DateTime _privGen;
    private int _currentAsteroidsCount;
}