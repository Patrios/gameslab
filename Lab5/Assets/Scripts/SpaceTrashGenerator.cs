﻿using System.Linq;
using UnityEngine;

public class SpaceTrashGenerator : MonoBehaviour
{
    // Update is called once per frame
    private void Update()
    {
        CollectBalls();
        GenerateBalls();
    }

    private void GenerateBalls()
    {
        for (var i = _currentBallsCount; i < BallsCount; i++)
        {
            _currentBallsCount++;
            var zPosition = Random.Range(
                Camera.transform.position.z + GenerationDistance,
                Camera.transform.position.z + 3 * GenerationDistance);

            var yPosition = Random.Range(BottomLeftGenerationPoint.y, UpperRightGenerationPoint.y);
            var xPosition = Random.Range(BottomLeftGenerationPoint.x, UpperRightGenerationPoint.x);

            Instantiate(BallPFB, new Vector3(xPosition, yPosition, zPosition), Quaternion.identity);
        }
    }

    private void CollectBalls()
    {
        var balls = GameObject
            .FindGameObjectsWithTag("Ball")
            .Where(ball => ball.transform.position.z + TrashDelta < Camera.transform.position.z);

        foreach (var ball in balls)
        {
            Destroy(ball);
            _currentBallsCount--;
        }
    }

    public float TrashDelta = 1f;

    public int BallsCount = 20;

    public float GenerationDistance = 50;

    public GameObject BallPFB;
    public GameObject Camera;

    public Vector2 BottomLeftGenerationPoint = new Vector2(-5f, -3f);
    public Vector2 UpperRightGenerationPoint = new Vector2(5f, 3f);

    private int _currentBallsCount = 0;
}