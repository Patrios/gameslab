﻿using UnityEngine;

public class Runner : MonoBehaviour
{
    // Update is called once per frame
    private void FixedUpdate()
    {
        var delta = RunningSpeed*Time.deltaTime;

        if (inFront)
        {
            delta *= -1;
        }
        
        transform.position = new Vector3(
            transform.position.x,
            transform.position.y,
            transform.position.z + delta);
    }

    public float RunningSpeed = 1f;

    public bool inFront = true;
}