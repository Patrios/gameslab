﻿using UnityEngine;

public class SpaceShipMoovment : MonoBehaviour                                                                  
{
    private void FixedUpdate()
    {
        var mousePosition = Input.mousePosition;
        mousePosition.z = 6;

        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Debug.Log(transform.position.z - Camera.main.gameObject.transform.position.z - 6);

        var rotationCenter = GameObject.FindGameObjectWithTag("RotationCenter");
        // rigidbody.rotation.SetFromToRotation(transform.position, mousePosition);

        transform.RotateAround(
            rotationCenter.transform.position,
            transform.up, 
            (mousePosition.x - transform.position.x) * AngularSpeed);

        transform.RotateAround(
            rotationCenter.transform.position,
            -transform.right,
            (mousePosition.y - transform.position.y) * AngularSpeed);

        var baseVelocity = transform.forward * Velocity;

        baseVelocity.z += (transform.position.z - Camera.main.gameObject.transform.position.z - 6)*BackVelocity * -1;

        rigidbody.velocity = baseVelocity;

    }

    public float Velocity;
    public float AngularSpeed;
    public float BackVelocity;
}