﻿using UnityEngine;

public class Shaker : MonoBehaviour 
{

	// Update is called once per frame
	void Update() 
    {
        if (ShakeIntensity > 0)
        {
            transform.position = OriginalPos + Random.insideUnitSphere * ShakeIntensity;
            transform.rotation = new Quaternion(OriginalRot.x + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f,
                                      OriginalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f,
                                      OriginalRot.z + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f,
                                      OriginalRot.w + Random.Range(-ShakeIntensity, ShakeIntensity) * .2f);

            ShakeIntensity -= ShakeDecay;
        }
        else if (Shaking)
        {
            Shaking = false;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Asteroid")
        {
            DoShake();
        }
    }

    public void DoShake()
    {
        OriginalPos = transform.position;
        OriginalRot = transform.rotation;

        ShakeIntensity = 0.4f;
        Shaking = true;
    }   

    public bool Shaking;
    private float ShakeDecay = 0.02f;
    private float ShakeIntensity;
    private Vector3 OriginalPos;
    private Quaternion OriginalRot;
}
