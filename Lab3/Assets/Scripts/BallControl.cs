﻿using UnityEngine;

public class BallControl : MonoBehaviour
{
    public void Awake()
    {
        StartGame();
    }

    public void StartGame()
    {
        var quat = Quaternion.Euler(0f, 0f, 135f + (Random.Range(0, 2) == 0 ? 0 : 90f));

        var currentPosition = rigidbody2D.transform.position;
        var ballVector = new Vector2(currentPosition.x, currentPosition.y);

        SetDirection(quat*ballVector);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Hey!");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Hello");

        rigidbody2D.velocity = Vector2.zero;

        var collisionNormal = collision.contacts[0].normal;

        SetDirection(Vector3.Reflect(_direction, collisionNormal));
    }

    public float StartForce;

    private void SetDirection(Vector2 inDirection)
    {
        _direction = inDirection;
        _direction.Normalize();

        rigidbody2D.AddForce(_direction * StartForce);
    }


    private Vector2 _direction;
}
