﻿using UnityEngine;

public class LoosLineControl : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ball")
        {
            Debug.Log("Loos entered");
            GameObject.Find("GameStateManager").GetComponent<GameStateControl>().GameLoos();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            Debug.Log("Collision Loos entered");
        }
    }
}