﻿using System.Linq;
using Assets.Scripts;
using UnityEngine;

public class GameStateControl : MonoBehaviour
{
    void FixedUpdate()
    {
        if (GameState == GameStates.MainGame)
        {
            var bricks = GameObject.FindGameObjectsWithTag("Brick");

            if (!bricks.Any())
            {
                OverGame("You Win!");
            }
        }
        else if(GameState == GameStates.GameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ResetGame();
            }
        }
    }

    public void GameLoos()
    {
        OverGame("You Loos =(");
    }

    private void OverGame(string message)
    {
        GameText.GetComponent<GUIText>().text = message;
        Destroy(GameObject.FindGameObjectWithTag("Ball"));
        GameState = GameStates.GameOver;
    }

    public GameObject GameText;

    public GameStates GameState;

    private void ResetGame()
    {
        Destroy(GameObject.FindGameObjectWithTag("Bricks"));

        var platePosition = GameObject.Find("Plate").transform.position;

        Instantiate(
            BallPFB, 
            new Vector3(platePosition.x, platePosition.y + 0.3f, platePosition.z), 
            Quaternion.identity);

        Instantiate(BriksPFB);  

        GameText.GetComponent<GUIText>().text = "";

        GameState = GameStates.MainGame;

        Debug.Log("Reset Game");
    }

    public GameObject BallPFB;

    public GameObject BriksPFB;
}