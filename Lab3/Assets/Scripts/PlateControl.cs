﻿using UnityEditorInternal;
using UnityEngine;

public class PlateControl : MonoBehaviour
{
    private void Awake()
    {
        _yPos = transform.position.y;
    }

    private void FixedUpdate()
    {
        var mousePosition = Input.mousePosition;
        var move = Camera.ScreenToWorldPoint(mousePosition).x;

        rigidbody2D.velocity = new Vector2(move * MaxSpeed, rigidbody2D.velocity.y);
        transform.position = new Vector3(transform.position.x, _yPos);
    }

    public float MaxSpeed;

    public Camera Camera;

    private float _yPos;
}